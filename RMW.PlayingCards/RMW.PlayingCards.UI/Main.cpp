
//Playing Cards
//Forked by Abby Thompson (Original by Reece Withall)

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank
{
	Two = 2,
	Three,
	Four, 
	Five,
	Six, 
	Seven,
	Eight, 
	Nine,
	Ten, 
	Jack, 
	Queen,
	King, 
	Ace 
};

enum Suit
{
	SPADES,
	HEARTS,
	CLUBS,
	DIAMONDS
};

struct Card
{
	Rank Rank;
	Suit Suit;

};

void PrintCard(Card card)
{
	cout << "The " << card.Rank << " of " << card.Suit;
}

Card HighCard(Card card1, Card card2)
{
	if (card1.Rank > card2.Rank)
		return card1;
	return card2;
}

int main()
{


	(void)_getch();
	return 0;
}